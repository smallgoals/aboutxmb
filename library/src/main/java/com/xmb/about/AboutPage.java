package com.xmb.about;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.DrawableRes;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v4.widget.TextViewCompat;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.Calendar;

import mehdi.sakout.aboutpage.R;

/**
 * The main class of this library with many predefined methods to add Elements for common items in
 * an About page. This class creates a {@link android.view.View} that can be passed as the root view
 * in {@link Fragment#onCreateView(LayoutInflater, ViewGroup, Bundle)} or passed to the
 * {@link android.app.Activity#setContentView(View)}
 * in an activity's {@link android.app.Activity#onCreate(Bundle)} method
 * <p>
 * To create a custom item in the about page, pass an instance of
 * {@link Element}
 * to the {@link AboutPage#addItem(Element)} method.
 *
 * @see Element
 */
public class AboutPage {
    private final Context mContext;
    private final LayoutInflater mInflater;
    private final View mView;
    private String mDescription;
    private int mImage = 0;
    private int imageWidthDP = 110;
    private int imageHeightDP = 110;
    private boolean mIsRTL = false;
    private Typeface mCustomFont;

    /**
     * The AboutPage requires a context to perform it's functions. Give it a context associated
     * to an
     * Activity or a Fragment. To avoid memory leaks, don't pass a
     * {@link android.content.Context#getApplicationContext() Context.getApplicationContext()} here.
     *
     * @param context
     */
    public AboutPage(Context context) {
        this.mContext = context;
        this.mInflater = LayoutInflater.from(context);
        this.mView = mInflater.inflate(R.layout.about_page, null);
    }

    /**
     * Provide a valid path to a font here to use another font for the text inside this AboutPage
     *
     * @param path
     * @return this AboutPage instance for builder pattern support
     */
    public AboutPage setCustomFont(String path) {
        //TODO: check if file exists
        mCustomFont = Typeface.createFromAsset(mContext.getAssets(), path);
        return this;
    }

    /**
     * Convenience method for {@link AboutPage#addEmail(java.lang.String, java.lang.String)} but
     * with
     * a predefined title string
     *
     * @param email the email address to send to
     * @return this AboutPage instance for builder pattern support
     */
    public AboutPage addEmail(String email) {
        return addEmail(email, mContext.getString(R.string.about_contact_email));
    }

    /**
     * Add a predefined Element that opens the users default email client with a new email to the
     * email address passed as parameter
     *
     * @param email the email address to send to
     * @param title the title string to display on this item
     * @return this AboutPage instance for builder pattern support
     */
    public AboutPage addEmail(String email, String title) {
        Element emailElement = new Element();
        emailElement.setTitle(title);
        emailElement.setIconDrawable(R.drawable.ic_email);
        emailElement.setIconTint(R.color.about_icon_color);

        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("message/rfc822");
        intent.putExtra(Intent.EXTRA_EMAIL, new String[]{email});
        emailElement.setIntent(intent);

        addItem(emailElement);
        return this;
    }

    /**
     * 意见反馈
     */
    public AboutPage addFeedback(View.OnClickListener lsn) {
        return addFeedback(mContext.getString(R.string.about_feedback), lsn);
    }

    public AboutPage addFeedback(String title, View.OnClickListener lsn) {
        Element feedbackElement = new Element();
        feedbackElement.setTitle(title);
        feedbackElement.setIconDrawable(R.drawable.ic_feed_back);
        feedbackElement.setIconTint(R.color.about_icon_color);
        feedbackElement.setOnClickListener(lsn);
        addItem(feedbackElement);
        return this;
    }

    /**
     * 声明
     */
    public AboutPage addWarning(View.OnClickListener lsn) {
        return addWarning(mContext.getString(R.string.about_warning), lsn);
    }


    public AboutPage addWarning(String title, View.OnClickListener lsn) {
        Element element = new Element();
        element.setTitle(title);
        element.setIconDrawable(R.drawable.ic_warning);
        element.setIconTint(R.color.about_icon_color);
        element.setOnClickListener(lsn);
        addItem(element);
        return this;
    }

    /**
     * 软件不错，点这里五星好评
     */
    public AboutPage addFiveStar() {
        return addFiveStar(mContext.getString(R.string.about_play_store));
    }

    public AboutPage addFiveStar(String title) {
        String id = mContext.getPackageName();
        Element playStoreElement = new Element();
        playStoreElement.setTitle(title);
        playStoreElement.setIconDrawable(R.drawable.ic_star);
        playStoreElement.setIconTint(R.color.about_icon_color);
        playStoreElement.setValue(id);
        Uri uri = Uri.parse("market://details?id=" + id);
        Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
        playStoreElement.setIntent(goToMarket);
        addItem(playStoreElement);
        return this;
    }

    /**
     * 检查更新
     */
    public AboutPage addCheckUpdate(View.OnClickListener lsn) {
        return addCheckUpdate(mContext.getString(R.string.about_check_update), lsn);
    }

    public AboutPage addCheckUpdate(String title, View.OnClickListener lsn) {
        Element element = new Element();
        element.setTitle(title);
        element.setIconDrawable(R.drawable.ic_update);
        element.setIconTint(R.color.about_icon_color);
        element.setOnClickListener(lsn);
        addItem(element);
        return this;
    }

    public AboutPage addShare(String text) {
        addShare(mContext.getString(R.string.about_share), text);
        return this;
    }


    /**
     * 分享
     */
    public AboutPage addShare(String title, String text) {
        Element gitHubElement = new Element();
        gitHubElement.setTitle(title);
        gitHubElement.setIconDrawable(R.drawable.ic_share);
        gitHubElement.setIconTint(R.color.about_icon_color);
        gitHubElement.setValue(text);

        Intent textIntent = new Intent(Intent.ACTION_SEND);
        textIntent.setType("text/plain");
        textIntent.putExtra(Intent.EXTRA_TEXT, text);

        gitHubElement.setIntent(textIntent);
        addItem(gitHubElement);

        return this;
    }

    /**
     * Convenience method for {@link AboutPage#addWebsite(String, String)} but with
     * a predefined title string
     *
     * @param url the URL to open in a browser
     * @return this AboutPage instance for builder pattern support
     */
    public AboutPage addWebsite(String url) {
        return addWebsite(url, mContext.getString(R.string.about_website));
    }

    /**
     * Add a predefined Element that the opens a browser and loads the specified URL
     *
     * @param url   the URL to open in a browser
     * @param title the title to display on this item
     * @return this AboutPage instance for builder pattern support
     */
    public AboutPage addWebsite(String url, String title) {
        if (!url.startsWith("http://") && !url.startsWith("https://")) {
            url = "http://" + url;
        }
        Element websiteElement = new Element();
        websiteElement.setTitle(title);
        websiteElement.setIconDrawable(R.drawable.ic_link);
        websiteElement.setIconTint(R.color.about_icon_color);
        websiteElement.setValue(url);

        Uri uri = Uri.parse(url);
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, uri);

        websiteElement.setIntent(browserIntent);
        addItem(websiteElement);

        return this;
    }


    public AboutPage addCopyRights() {
        String s = mContext.getString(R.string.copy_right);
        String copyrights = String.format(s, Calendar
                .getInstance().get(Calendar.YEAR));
        addCopyRights(copyrights);
        return this;
    }

    public AboutPage addCopyRights(String text) {
        Element copyRightsElement = new Element();
        copyRightsElement.setTitle(text);
        copyRightsElement.setIconDrawable(R.drawable.about_icon_copy_right);
        copyRightsElement.setIconTint(mehdi.sakout.aboutpage.R.color.about_item_icon_color);
        copyRightsElement.setIconNightTint(android.R.color.white);
        copyRightsElement.setGravity(Gravity.CENTER);
        addItem(copyRightsElement);
        return this;
    }


    /**
     * Add a custom {@link Element} to this AboutPage
     *
     * @param element
     * @return this AboutPage instance for builder pattern support
     * @see Element
     */
    public AboutPage addItem(Element element) {
        LinearLayout wrapper = (LinearLayout) mView.findViewById(R.id.about_providers);
        wrapper.addView(createItem(element));
        wrapper.addView(getSeparator(), new ViewGroup.LayoutParams(ViewGroup.LayoutParams
                .MATCH_PARENT, mContext.getResources().getDimensionPixelSize(R.dimen
                .about_separator_height)));
        return this;
    }

    /**
     * Set the header image to display in this AboutPage
     *
     * @param resource the resource id of the image to display
     * @return this AboutPage instance for builder pattern support
     */
    public AboutPage setImage(@DrawableRes int resource) {
        this.mImage = resource;
        return this;
    }

    public AboutPage setImage(@DrawableRes int resource, int imageWidthDP, int imageHeightDP) {
        this.mImage = resource;
        this.imageWidthDP = imageWidthDP;
        this.imageHeightDP = imageHeightDP;
        return this;
    }

    /**
     * Add a new group that will display a header in this AboutPage
     * <p>
     * A header will be displayed in the order it was added. For e.g:
     * <p>
     * <code>
     * new AboutPage(this)
     * .addItem(firstItem)
     * .addGroup("Header")
     * .addItem(secondItem)
     * .create();
     * </code>
     * <p>
     * Will display the following
     * [First item]
     * [Header]
     * [Second item]
     *
     * @param name the title for this group
     * @return this AboutPage instance for builder pattern support
     */
    public AboutPage addGroup(String name) {

        TextView textView = new TextView(mContext);
        textView.setText(name);
        TextViewCompat.setTextAppearance(textView, R.style.about_groupTextAppearance);
        LinearLayout.LayoutParams textParams = new LinearLayout.LayoutParams(ViewGroup
                .LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        if (mCustomFont != null) {
            textView.setTypeface(mCustomFont);
        }

        int padding = mContext.getResources().getDimensionPixelSize(R.dimen
                .about_group_text_padding);
        textView.setPadding(padding, padding, padding, padding);


        if (mIsRTL) {
            textView.setGravity(Gravity.END | Gravity.CENTER_VERTICAL);
            textParams.gravity = Gravity.END | Gravity.CENTER_VERTICAL;
        } else {
            textView.setGravity(Gravity.START | Gravity.CENTER_VERTICAL);
            textParams.gravity = Gravity.START | Gravity.CENTER_VERTICAL;
        }
        textView.setLayoutParams(textParams);

        ((LinearLayout) mView.findViewById(R.id.about_providers)).addView(textView);
        return this;
    }

    /**
     * Turn on the RTL mode.
     *
     * @param value
     * @return this AboutPage instance for builder pattern support
     */
    public AboutPage isRTL(boolean value) {
        this.mIsRTL = value;
        return this;
    }

    public AboutPage setDescription(String description) {
        this.mDescription = description;
        return this;
    }

    /**
     * Create and inflate this AboutPage. After this method is called the AboutPage
     * cannot be customized any more.
     *
     * @return the inflated {@link View} of this AboutPage
     */
    public View create() {
        TextView description = (TextView) mView.findViewById(R.id.description);
        ImageView image = (ImageView) mView.findViewById(R.id.image);
        if (mImage > 0) {
            image.setImageResource(mImage);
            ViewGroup.LayoutParams params = image.getLayoutParams();
            params.height = dip2px(imageWidthDP);
            params.width = dip2px(imageHeightDP);
            image.setLayoutParams(params);
        }

        if (!TextUtils.isEmpty(mDescription)) {
            description.setText(mDescription);
        }

        description.setGravity(Gravity.CENTER);

        if (mCustomFont != null) {
            description.setTypeface(mCustomFont);
        }

        return mView;
    }

    private int dip2px(float dipValue) {
        final float scale = mContext.getResources().getDisplayMetrics().density;
        return (int) (dipValue * scale + 0.5f);
    }

    private View createItem(final Element element) {
        LinearLayout wrapper = new LinearLayout(mContext);
        wrapper.setOrientation(LinearLayout.HORIZONTAL);
        wrapper.setClickable(true);

        if (element.getOnClickListener() != null) {
            wrapper.setOnClickListener(element.getOnClickListener());
        } else if (element.getIntent() != null) {
            wrapper.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        mContext.startActivity(element.getIntent());
                    } catch (Exception e) {
                    }
                }
            });
        }
        if (element.getOnLongClickListener() != null) {
            wrapper.setOnLongClickListener(element.getOnLongClickListener());
        }
        TypedValue outValue = new TypedValue();
        mContext.getTheme().resolveAttribute(R.attr.selectableItemBackground, outValue, true);
        wrapper.setBackgroundResource(outValue.resourceId);

        int padding = mContext.getResources().getDimensionPixelSize(R.dimen.about_text_padding);
        wrapper.setPadding(padding, padding, padding, padding);
        LinearLayout.LayoutParams wrapperParams = new LinearLayout.LayoutParams(ViewGroup
                .LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        wrapper.setLayoutParams(wrapperParams);


        TextView textView = new TextView(mContext);
        TextViewCompat.setTextAppearance(textView, R.style.about_elementTextAppearance);
        LinearLayout.LayoutParams textParams = new LinearLayout.LayoutParams(ViewGroup
                .LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        textView.setLayoutParams(textParams);
        if (mCustomFont != null) {
            textView.setTypeface(mCustomFont);
        }

        ImageView iconView = null;

        if (element.getIconDrawable() != null) {
            iconView = new ImageView(mContext);
            int size = mContext.getResources().getDimensionPixelSize(R.dimen.about_icon_size);
            LinearLayout.LayoutParams iconParams = new LinearLayout.LayoutParams(size, size);
            iconView.setLayoutParams(iconParams);
            int iconPadding = mContext.getResources().getDimensionPixelSize(R.dimen
                    .about_icon_padding);
            iconView.setPadding(iconPadding, 0, iconPadding, 0);

            if (Build.VERSION.SDK_INT < 21) {
//                Drawable drawable = VectorDrawableCompat.create(iconView.getResources(), element
//                        .getIconDrawable(), iconView.getContext().getTheme());
//                iconView.setImageDrawable(drawable);
                iconView.setImageResource(element.getIconDrawable());
            } else {
                iconView.setImageResource(element.getIconDrawable());
            }

            Drawable wrappedDrawable = DrawableCompat.wrap(iconView.getDrawable());
            wrappedDrawable = wrappedDrawable.mutate();
            if (element.getAutoApplyIconTint()) {
                int currentNightMode = mContext.getResources().getConfiguration().uiMode
                        & Configuration.UI_MODE_NIGHT_MASK;
                if (currentNightMode != Configuration.UI_MODE_NIGHT_YES) {
                    if (element.getIconTint() != null) {
                        DrawableCompat.setTint(wrappedDrawable, ContextCompat.getColor(mContext,
                                element.getIconTint()));
                    } else {
                        DrawableCompat.setTint(wrappedDrawable, ContextCompat.getColor(mContext,
                                R.color.about_item_icon_color));
                    }
                } else if (element.getIconNightTint() != null) {
                    DrawableCompat.setTint(wrappedDrawable, ContextCompat.getColor(mContext,
                            element.getIconNightTint()));
                } else {
                    DrawableCompat.setTint(wrappedDrawable, AboutPageUtils.getThemeAccentColor
                            (mContext));
                }
            }

        } else {
            int iconPadding = mContext.getResources().getDimensionPixelSize(R.dimen
                    .about_icon_padding);
            textView.setPadding(iconPadding, iconPadding, iconPadding, iconPadding);
        }


        textView.setText(element.getTitle());


        if (mIsRTL) {

            final int gravity = element.getGravity() != null ? element.getGravity() : Gravity.END;

            wrapper.setGravity(gravity | Gravity.CENTER_VERTICAL);
            //noinspection ResourceType
            textParams.gravity = gravity | Gravity.CENTER_VERTICAL;
            wrapper.addView(textView);
            if (element.getIconDrawable() != null) {
                wrapper.addView(iconView);
            }

        } else {
            final int gravity = element.getGravity() != null ? element.getGravity() : Gravity.START;
            wrapper.setGravity(gravity | Gravity.CENTER_VERTICAL);
            //noinspection ResourceType
            textParams.gravity = gravity | Gravity.CENTER_VERTICAL;
            if (element.getIconDrawable() != null) {
                wrapper.addView(iconView);
            }
            wrapper.addView(textView);
        }

        return wrapper;
    }

    private View getSeparator() {
        return mInflater.inflate(R.layout.about_page_separator, null);
    }
}
