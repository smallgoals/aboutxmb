# Android About Page
Create an awesome About Page for your Android App in 2 minutes

<img src="/resources/cover.png" width="80%" alt="Android About Page Cover"/>

This library allows to generate beautiful About Pages with less effort, it's fully customizable and supports opening specific intent

```java
public class MainActivity extends AppCompatActivity {
    //<color name="about_icon_color">@color/colorPrimary</color>小图标颜色，
    //<color name="about_background_color">#FFFFFF</color>背景色

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        simulateDayNight(/* DAY */ 0);

        View aboutPage = new AboutPage(this)
                .isRTL(false)
                .setDescription(getString(R.string.app_name))
                .setImage(R.drawable.ic_launcher)
                .addGroup("版本")
                .addItem(new Element().setTitle("Version " + getAppVersionName(this))
                        .setOnLongClickListener(new View
                                .OnLongClickListener() {
                            @Override
                            public boolean onLongClick(View v) {
                                Toast.makeText(MainActivity.this, "Yes!!", Toast.LENGTH_SHORT)
                                        .show();
                                return false;
                            }
                        }))
                .addGroup("关于")
                .addFeedback()// 自定义文字：addFeedback("意见反馈")
                .addFiveStar()//自定义文字：addFeedback(title)
                .addEmail("glxiaomubiao@163.com")//自定义文字：addEmail(email, title)
                .addWebsite("http://xiaomubiaokeji.com/")//自定义文字：addWebsite(String url, String
                // title)
                .addWarning()// or: addWarning(String title)
                .addCheckUpdate()// or: addCheckUpdate(title)
                .addShare("分享APP", "发现一个好APP：" + getApplicationInfo().loadLabel(getPackageManager
                        ()))
                .addItem(getTestElement())//自定义Item
                .addCopyRights()// or: addCopyRights(String text)
                .create();

        setContentView(aboutPage);
    }

    Element getTestElement() {
        Element copyRightsElement = new Element();
        copyRightsElement.setTitle("自定义item");
        copyRightsElement.setIconDrawable(R.drawable.ic_launcher);
        copyRightsElement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MainActivity.this, "setOnClickListener", Toast.LENGTH_SHORT).show();
            }
        });
        copyRightsElement.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                Toast.makeText(MainActivity.this, "onLongClick", Toast.LENGTH_SHORT).show();
                return false;
            }
        });
        return copyRightsElement;
    }

    public String getAppVersionName(Context context) {
        String versionName = "";
        try {
            versionName = context.getPackageManager().getPackageInfo(
                    context.getPackageName(), 0).versionName;
        } catch (Exception e) {
        }
        return versionName;
    }

    void simulateDayNight(int currentSetting) {
        final int DAY = 0;
        final int NIGHT = 1;
        final int FOLLOW_SYSTEM = 3;

        int currentNightMode = getResources().getConfiguration().uiMode
                & Configuration.UI_MODE_NIGHT_MASK;
        if (currentSetting == DAY && currentNightMode != Configuration.UI_MODE_NIGHT_NO) {
            AppCompatDelegate.setDefaultNightMode(
                    AppCompatDelegate.MODE_NIGHT_NO);
        } else if (currentSetting == NIGHT && currentNightMode != Configuration.UI_MODE_NIGHT_YES) {
            AppCompatDelegate.setDefaultNightMode(
                    AppCompatDelegate.MODE_NIGHT_YES);
        } else if (currentSetting == FOLLOW_SYSTEM) {
            AppCompatDelegate.setDefaultNightMode(
                    AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM);
        }
    }
}
```

## Setup
Add it in your root build.gradle at the end of repositories:

	allprojects {
		repositories {
			...
			maven { url 'https://jitpack.io' }
		}
	}Copy
Step 2. Add the dependency

	dependencies {
	        compile 'org.bitbucket.smallgoals:aboutxmb:1.0'
	}
```


